# 1.19.4-v1.4.1
Ported to 1.19.4

## Changes
- Buffed the Soul Cannon damage a little.
- The Soul Cannon has a shorter cooldown when in creative mode.