package com.legacy.dungeons_plus.registry;

import javax.annotation.Nullable;

import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.core.RegistryAccess;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;

public class DPDamageSources
{
	@Nullable
	public static DPDamageSources instance;

	private final Registry<DamageType> damageTypes;
	private final DamageSource consumeSoul;

	public DPDamageSources(RegistryAccess registryAccess)
	{
		this.damageTypes = registryAccess.registryOrThrow(Registries.DAMAGE_TYPE);
		this.consumeSoul = this.source(DPDamageTypes.CONSUME_SOUL.getKey());
	}

	private DamageSource source(ResourceKey<DamageType> type)
	{
		return new DamageSource(this.damageTypes.getHolderOrThrow(type));
	}

	private DamageSource source(ResourceKey<DamageType> type, @Nullable Entity causingEntity)
	{
		return new DamageSource(this.damageTypes.getHolderOrThrow(type), causingEntity);
	}

	private DamageSource source(ResourceKey<DamageType> type, @Nullable Entity directEntity, @Nullable Entity causingEntity)
	{
		return new DamageSource(this.damageTypes.getHolderOrThrow(type), directEntity, causingEntity);
	}

	public DamageSource consumeSoul()
	{
		return this.consumeSoul;
	}

	public DamageSource thrownItem(Entity directEntity, @Nullable Entity causingEntity, ItemStack thrownItem)
	{
		return new ThrownItemDamageSource(this.damageTypes.getHolderOrThrow(DPDamageTypes.WARPED_AXE.getKey()), directEntity, causingEntity, thrownItem);
	}

	public static class ThrownItemDamageSource extends DamageSource
	{
		@Nullable
		private final ItemStack thrownItem;

		public ThrownItemDamageSource(Holder<DamageType> type, @Nullable Entity directEntity, @Nullable Entity causingEntity, @Nullable ItemStack thrownItem)
		{
			super(type, directEntity, causingEntity);
			this.thrownItem = thrownItem;
		}

		@Override
		public Component getLocalizedDeathMessage(LivingEntity killedEntity)
		{
			Component attackerName = this.getEntity() == null ? this.getDirectEntity().getDisplayName() : this.getEntity().getDisplayName();
			ItemStack stack = this.thrownItem == null ? ItemStack.EMPTY : this.thrownItem;
			String msg = "death.attack." + this.getMsgId();
			String msgWithItem = msg + ".item";
			return !stack.isEmpty() && stack.hasCustomHoverName() ? Component.translatable(msgWithItem, killedEntity.getDisplayName(), attackerName, stack.getDisplayName()) : Component.translatable(msg, killedEntity.getDisplayName(), attackerName);
		}

	}
}
