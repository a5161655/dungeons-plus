package com.legacy.dungeons_plus.registry;

import com.legacy.dungeons_plus.DungeonsPlus;
import com.legacy.structure_gel.api.registry.registrar.Registrar;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.damagesource.DamageType;

public class DPDamageTypes
{
	public static final RegistrarHandler<DamageType> HANDLER = RegistrarHandler.getOrCreate(Registries.DAMAGE_TYPE, DungeonsPlus.MODID);
	
	public static final Registrar.Pointer<DamageType> WARPED_AXE = HANDLER.createPointer("warped_axe", () -> new DamageType(name("warped_axe"), 0.1F));
	public static final Registrar.Pointer<DamageType> CONSUME_SOUL = HANDLER.createPointer("consume_soul", () -> new DamageType(name("consume_soul"), 0.0F));

	private static String name(String key)
	{
		return DungeonsPlus.MODID + "." + key;
	}
}
